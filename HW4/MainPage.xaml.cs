﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    //[DesignTimeVisible(false)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage: ContentPage
    {
        public class EntryItem
        {
           public string Title { set; get; }
           public string Detail { set; get; }
            public string Translated { set; get; }
            public EntryItem(string v1, string v2, string v3)
            {
                Title = v1;
                Detail = v2;
                Translated = v3;
            }
        }
        public MainPage()
        {
           InitializeComponent();

            Buildlist();


        }

        private void Buildlist()
        {
            List<EntryItem> Items = new List<EntryItem>
            {
                new EntryItem("Hostia","catholic munchies", "Communion Wafer"),
                new EntryItem("Coger","to acquire, with a connotation of force", "To take"),
                new EntryItem("Hedionda","take a bath", "Malodorous"),
                new EntryItem("Paja","a favored snack down by the farm", "straw"),
                new EntryItem("Aparcar","sounds a bit like a slurred version of the end result", "Park") //a parked car
            };
            //credit to Dulce Robles for words and translations
            MyListView.ItemsSource = Items;
        }

        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            await DisplayAlert("A transgression noted!", "RUDE to just be tapping on people like that! Try reading the instructions.", "My bad");

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }

        private void MyListView_Refreshing(object sender, EventArgs e)
        {
            Buildlist();
            MyListView.IsRefreshing = false;
        }

       async void ViewT_Clicked(object sender, EventArgs e)
        {
            // The sender is the menuItem
            MenuItem menuItem = sender as MenuItem;
      
            EntryItem theItem = (EntryItem)menuItem.CommandParameter;
            
            if (theItem == null)
                return;

            Navigation.PushAsync(new Page1(theItem.Translated));
        }
    }

}
